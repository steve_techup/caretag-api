﻿using Caretag.Api.Services.Implementations;
using Caretag.Api.Services.Interfaces;
using Caretag.Data.Models;
using Caretag.Data.Repositories.Imlementations;
using Caretag.Data.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Caretag.Api.Extensions
{
    public static class ConfigurationExtensions
    {
        #region Database Context

        /// <summary>
        /// Configure DB context for DI
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public static void ConfigureDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            string connString = configuration["ConnectionStrings:CaretegDb"];
            services.AddDbContext<DatabaseContext>(options => options.UseMySql(connString, ServerVersion.AutoDetect(connString)));
        }

        #endregion

        #region Register Services

        #region Repositories
       /// <summary>
       /// Configure repositoris for DI
       /// </summary>
       /// <param name="services"></param>
        public static void RegisterRepositories(this IServiceCollection services)
        {
            services.AddScoped<ISurgicalInstrumentRepository, SurgicalInstrumentRepository>();
        }
        #endregion

        #region Services
        /// <summary>
        /// Configure services for DI
        /// </summary>
        /// <param name="services"></param>
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<ISurgicalInstrumentService, SurgicalInstrumentService>();
        }
        #endregion

        #endregion
    }
}
