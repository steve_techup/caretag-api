﻿using AutoMapper;
using Caretag.Api.Exceptions;
using Caretag.Api.Services.Interfaces;
using Caretag.Data.Models;
using Caretag.Data.Repositories.Interfaces;
using Caretag.DTO.SurgicalInstrument;

namespace Caretag.Api.Services.Implementations
{
    public class SurgicalInstrumentService : ISurgicalInstrumentService
    {
        #region Constructors & DI
        private readonly ISurgicalInstrumentRepository _surgicalInstrumentRepository;
        private readonly IMapper _mapper;
        public SurgicalInstrumentService(ISurgicalInstrumentRepository surgicalInstrumentRepository, IMapper mapper)
        {
            _surgicalInstrumentRepository = surgicalInstrumentRepository;
            _mapper = mapper;
        }
        #endregion

        /// <summary>
        /// Create surgical instrument
        /// </summary>
        /// <param name="surgicalInstrument"></param>
        /// <returns></returns>
        public async Task Add(SurgicalInstrumentBase surgicalInstrument)
        {
            await _surgicalInstrumentRepository.Add(_mapper.Map<SurgicalInstrument>(surgicalInstrument));
        }

        /// <summary>
        /// Update surgical instrument
        /// </summary
        /// <param name="surgicalInstrument"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task Update(SurgicalInstrumentBase surgicalInstrument, int id)
        {
            var _surgicalInstrument = await _surgicalInstrumentRepository.Get(id);

            if (_surgicalInstrument == null)
            {
                throw new InvalidEntityException($"Surgical instrument with ID: {id} was not found");
            }

            _surgicalInstrument.Name = surgicalInstrument.Name;
            _surgicalInstrument.Description = surgicalInstrument.Description;
            _surgicalInstrument.ImageUrl = surgicalInstrument.ImageUrl;
            _surgicalInstrument.LastUpdated = DateTime.Now;

            await _surgicalInstrumentRepository.Update(_surgicalInstrument);
        }

        /// <summary>
        /// Get surgical instrument by passing the ID as a parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<SurgicalInstrumentDto> Get(int id)
        {
            return _mapper.Map<SurgicalInstrumentDto>(await _surgicalInstrumentRepository.Get(id));
        }

        /// <summary>
        /// Remove a surgical instrument
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task Remove(int id)
        {
            var _surgicalInstrument = await _surgicalInstrumentRepository.Get(id);

            if (_surgicalInstrument == null)
            {
                throw new InvalidEntityException($"Surgical instrument with ID: {id} was not found");
            }

            await _surgicalInstrumentRepository.Remove(_surgicalInstrument);
        }

        /// <summary>
        /// Search for all surgical instruments whose name contains the search text
        /// </summary>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public async Task<List<SurgicalInstrumentDto>> Search(string? searchText)
        {
            return _mapper.Map<List<SurgicalInstrumentDto>>(await _surgicalInstrumentRepository.Search(searchText));
        }
    }
}
