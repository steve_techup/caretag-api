﻿namespace Caretag.DTO.SurgicalInstrument
{
    public class SurgicalInstrumentDto : SurgicalInstrumentBase
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? LastUpdated{ get; set; }
    }
}
