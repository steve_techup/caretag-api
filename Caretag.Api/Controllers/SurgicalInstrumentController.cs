﻿using Caretag.Api.Exceptions;
using Caretag.Api.Services.Interfaces;
using Caretag.DTO.SurgicalInstrument;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Caretag.Api.Controllers
{
    [Route("api/surgical-instrument")]
    [ApiController]
    public class SurgicalInstrumentController : ControllerBase
    {
        #region Constructors & DI
        private readonly ISurgicalInstrumentService _surgicalInstrumentService;
        public SurgicalInstrumentController(ISurgicalInstrumentService surgicalInstrumentService)
        {
            _surgicalInstrumentService = surgicalInstrumentService;
        }
        #endregion

        /// <summary>
        /// Create an surgical instrument 
        /// </summary>
        /// <param name="surgicalInstrument"></param>
        /// <response code="204">An record was successfully created</response>
        [HttpPost]
        [ProducesResponseType(typeof(void), 204)]
        public async Task<IActionResult> Create([FromBody] SurgicalInstrumentBase surgicalInstrument)
        {
            await _surgicalInstrumentService.Add(surgicalInstrument);
            return NoContent();
        }

        /// <summary>
        /// Update surgical instrument 
        /// </summary>
        /// <param name="surgicalInstrument"></param>
        /// <param name="id"></param>
        /// <response code="204">Record with the given ID was successfully updated</response>
        /// <response code="404">There is no record with the given ID</response>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(void), 204)]
        [ProducesResponseType(typeof(string), 404)]
        public async Task<IActionResult> Update([FromBody] SurgicalInstrumentBase surgicalInstrument, [FromRoute][Required] int id)
        {
            try
            {
                await _surgicalInstrumentService.Update(surgicalInstrument, id);
                return NoContent();
            }
            catch (InvalidEntityException ex)
            {
                return NotFound(ex.Message);
            }
        }

        /// <summary>
        /// Get a surgical instrument 
        /// </summary>
        /// <param name="id"></param>
        /// <response code="200">Returns the record with the given ID</response>
        /// <response code="404">There is no record with the given ID</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(SurgicalInstrumentDto), 200)]
        [ProducesResponseType(typeof(string), 404)]
        public async Task<IActionResult> Get([FromRoute][Required] int id)
        {
            var surgicalInstrument = await _surgicalInstrumentService.Get(id);

            if (surgicalInstrument != null)
                return Ok(surgicalInstrument);
            else
                return NotFound($"Surgical instrument with ID: {id} was not found");
        }


        /// <summary>
        /// Search for a surgical instruments
        /// </summary>
        /// <param name="searchText"></param>
        /// <response code="200">Returns all record whose name contains the search text</response>
        [HttpGet("search")]
        [ProducesResponseType(typeof(List<SurgicalInstrumentDto>), 200)]
        public async Task<IActionResult> Search([FromQuery] string? searchText)
        {
            return Ok(await _surgicalInstrumentService.Search(searchText));
        }

        /// <summary>
        /// Delete surgical instrument 
        /// </summary>
        /// <param name="id"></param>
        /// <response code="204">Record with the given ID was successfully deleted</response>
        /// <response code="404">There is no record with the given ID</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(void), 204)]
        [ProducesResponseType(typeof(string), 404)]
        public async Task<IActionResult> Delete([FromRoute][Required] int id)
        {
            try
            {
                await _surgicalInstrumentService.Remove(id);
                return NoContent();
            }
            catch (InvalidEntityException ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}
