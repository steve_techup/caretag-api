﻿namespace Caretag.Api.Exceptions
{
    public class InvalidEntityException : Exception
    {
        public InvalidEntityException() : base() { }

        public InvalidEntityException(string message) : base(message) { }
    }
}
