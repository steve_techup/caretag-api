﻿using Caretag.Data.Models;
using Caretag.Data.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caretag.Data.Repositories.Imlementations
{
    public class SurgicalInstrumentRepository : ISurgicalInstrumentRepository
    {
        #region Constructors & DI
        private readonly DatabaseContext _db;
        public SurgicalInstrumentRepository(DatabaseContext db)
        {
            _db = db;
        }
        #endregion

        /// <summary>
        /// Get surgical instrument by passing the ID as a parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<SurgicalInstrument?> Get(int id)
        {
            return await _db.SurgicalInstruments.Where(si => si.Id == id).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Create surgical instrument
        /// </summary>
        /// <param name="surgicalInstrument"></param>
        /// <returns></returns>
        public async Task Add(SurgicalInstrument surgicalInstrument)
        {
            await _db.SurgicalInstruments.AddAsync(surgicalInstrument);
            await _db.SaveChangesAsync();
        }

        /// <summary>
        /// Update surgical instrument
        /// </summary>
        /// <param name="surgicalInstrument"></param>
        /// <returns></returns>
        public async Task Update(SurgicalInstrument surgicalInstrument)
        {
            _db.SurgicalInstruments.Update(surgicalInstrument);
            await _db.SaveChangesAsync();
        }

        /// <summary>
        /// Remove a surgical instrument
        /// </summary>
        /// <param name="surgicalInstrument"></param>
        /// <returns></returns>
        public async Task Remove(SurgicalInstrument surgicalInstrument)
        {
            _db.SurgicalInstruments.Remove(surgicalInstrument);
            await _db.SaveChangesAsync();
        }

        /// <summary>
        /// Search for all surgical instruments whose name contains the search text
        /// </summary>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public async Task<List<SurgicalInstrument>> Search(string searchText)
        {
            return await _db.SurgicalInstruments.Where(si => string.IsNullOrEmpty(searchText) || si.Name.ToLower().Contains(searchText.ToLower()))
                                                .ToListAsync();
        }


    }
}
