﻿using Caretag.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caretag.Data.Repositories.Interfaces
{
    public interface ISurgicalInstrumentRepository
    {
        Task<SurgicalInstrument?> Get(int id);
        Task Add(SurgicalInstrument surgicalInstrument);
        Task Update(SurgicalInstrument surgicalInstrument);
        Task Remove(SurgicalInstrument surgicalInstrument);
        Task<List<SurgicalInstrument>> Search(string searchText);
    }
}
