﻿using AutoMapper;
using Caretag.Data.Models;
using Caretag.DTO.SurgicalInstrument;

namespace Caretag.Api.Mappers
{
    public class SurgicalInstrumentMapper : Profile
    {
        public SurgicalInstrumentMapper()
        {
            CreateMap<SurgicalInstrumentBase, SurgicalInstrument>()
               .ForMember(d => d.Name, opt => opt.MapFrom(s => s.Name))
               .ForMember(d => d.Description, opt => opt.MapFrom(s => s.Description))
               .ForMember(d => d.ImageUrl, opt => opt.MapFrom(s => s.ImageUrl))
               .ForMember(d => d.DateCreated, opt => opt.MapFrom(s => DateTime.Now));

            CreateMap<SurgicalInstrument, SurgicalInstrumentDto>()
               .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
               .ForMember(d => d.Name, opt => opt.MapFrom(s => s.Name))
               .ForMember(d => d.Description, opt => opt.MapFrom(s => s.Description))
               .ForMember(d => d.ImageUrl, opt => opt.MapFrom(s => s.ImageUrl))
               .ForMember(d => d.DateCreated, opt => opt.MapFrom(s => s.DateCreated))
               .ForMember(d => d.LastUpdated, opt => opt.MapFrom(s => s.LastUpdated));
        }
    }
}
