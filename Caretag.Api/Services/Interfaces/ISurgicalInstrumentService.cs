﻿using Caretag.DTO.SurgicalInstrument;

namespace Caretag.Api.Services.Interfaces
{
    public interface ISurgicalInstrumentService
    {
        Task Add(SurgicalInstrumentBase surgicalInstrument);
        Task Update(SurgicalInstrumentBase surgicalInstrument, int id);
        Task<SurgicalInstrumentDto> Get(int id);
        Task Remove(int id);
        Task<List<SurgicalInstrumentDto>> Search(string? searchText);
    }
}
