﻿using System.ComponentModel.DataAnnotations;

namespace Caretag.DTO.SurgicalInstrument
{
    public class SurgicalInstrumentBase
    {
        [Required]
        [MaxLength(100)]
        public string Name { get; set; } = null!;
        [Required]
        [MaxLength(250)]
        public string Description { get; set; } = null!;
        [Required]
        [MaxLength(512)]
        public string ImageUrl { get; set; } = null!;
    }
}
