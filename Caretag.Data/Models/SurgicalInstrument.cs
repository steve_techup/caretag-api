﻿namespace Caretag.Data.Models
{
    public partial class SurgicalInstrument
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string Description { get; set; } = null!;
        public string ImageUrl { get; set; } = null!;
        public DateTime DateCreated { get; set; }
        public DateTime? LastUpdated { get; set; }
    }
}
